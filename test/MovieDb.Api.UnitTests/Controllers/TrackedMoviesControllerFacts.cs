﻿using AutoFixture;
using FakeItEasy;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using MovieDb.Api.Controllers;
using MovieDb.Api.Model;
using MovieDb.Data;
using MovieDb.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MovieDb.Api.UnitTests.Controllers
{
    public partial class TrackedMoviesControllerFacts : IDisposable
    {
        private SqliteConnection connection;
        private TrackedMoviesController controller;
        private MovieDbContext context;
        private Fixture fixture;
        private const string testUserId = "testUser";
        private const string fakeRoute = "/fake-route";

        public TrackedMoviesControllerFacts()
        {
            connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<MovieDbContext>()
                   .UseSqlite(connection)
                   .Options;

            // Create the schema in the database
            using (var context = new MovieDbContext(options))
            {
                context.Database.EnsureCreated();
            }

            // Run the test against this instance of the context
            // But should create another instance to assert against
            context = new MovieDbContext(options);
            controller = new TrackedMoviesController(context);

            var fakeUrlHelper = A.Fake<IUrlHelper>();
            A.CallTo(fakeUrlHelper)
                .Where(c => c.Method.Name == nameof(IUrlHelper.RouteUrl))
                .WithReturnType<string>()
                .Returns(fakeRoute);
            controller.Url = fakeUrlHelper;

            var fakeHttpContext = A.Fake<HttpContext>();
            var fakeResponse = A.Fake<HttpResponse>();
            A.CallTo(() => fakeHttpContext.Response).Returns(fakeResponse);
            A.CallTo(() => fakeResponse.Headers).Returns(new HeaderDictionary());
            controller.ControllerContext = new ControllerContext {HttpContext = fakeHttpContext };

            fixture = CreateOmitOnRecursionFixture();
            fixture.Customize<TrackedMovieToWatch>(ob => ob.With(x => x.UserId, testUserId));
        }


        public void Dispose()
        {
            ((IDisposable)connection).Dispose();
            context.Dispose();
        }

        protected async Task<(IEnumerable<TrackedMovieWatched> trackedMovies, IEnumerable<Movie> movies)> CreateWatched(int count = 3)
        {
            var watchedMovies = fixture.CreateMany<TrackedMovieWatched>(count).ToList();
            var movies = await CreateMovies(watchedMovies.Select(x => x.MovieId).ToArray());
            watchedMovies.ForEach(Customize);

            context.TrackedMovies.AddRange(watchedMovies);
            await context.SaveChangesAsync();

            return (watchedMovies, movies);

            void Customize(TrackedMovieWatched trackedMovie)
            {
                trackedMovie.UserId = testUserId;
                trackedMovie.WatchedHistory.ToList().ForEach(h =>
                {
                    h.MovieId = trackedMovie.MovieId;
                    h.UserId = trackedMovie.UserId;
                });
                trackedMovie.Movie = null;
            }
        }

        protected async Task<(IEnumerable<TrackedMovieToWatch> trackedMovies, IEnumerable<Movie> movies)> CreateToWatch(int count = 3)
        {
            var toWatchMovies = fixture.CreateMany<TrackedMovieToWatch>(count).ToList();
            var movies = await CreateMovies(toWatchMovies.Select(x => x.MovieId).ToArray());

            context.TrackedMovies.AddRange(toWatchMovies);
            await context.SaveChangesAsync();

            return (toWatchMovies, movies);

        }

        protected async Task<IEnumerable<Movie>> CreateMovies(params string[] movieIds)
        {
            var tempMovies = fixture.CreateMany<Movie>(movieIds.Length).ToList();
            for (int i = 0; i < movieIds.Length; i++)
            {
                tempMovies[i].MovieId = movieIds[i];
            }

            context.Movies.AddRange(tempMovies);
            await context.SaveChangesAsync();

            return tempMovies;
        }

        /// <summary>
        /// Creates OmitOnRecursionBehavior as opposite to ThrowingRecursionBehavior.
        /// </summary>
        /// <returns></returns>
        public static Fixture CreateOmitOnRecursionFixture()
        { //from https://github.com/AutoFixture/AutoFixture/issues/337
            var fixture = new Fixture();
            fixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList()
                             .ForEach(b => fixture.Behaviors.Remove(b));
            fixture.Behaviors.Add(new OmitOnRecursionBehavior()); //recursionDepth

            return fixture;
        }

        public class GetTrackedMovieShould : TrackedMoviesControllerFacts
        {
            [Fact]
            public async Task Returns_OK_When_Tracked_Movie_To_Watch_Exists()
            {
                // Arrange
                // Add existing tracked Movie
                var (toWatch, movies) = await CreateToWatch(1);

                // Act
                var result = await controller.GetTrackedMovie(toWatch.First().UserId, toWatch.First().Id);

                // Assert
                var okObjectResult = Assert.IsType<OkObjectResult>(result);
                var tracked = okObjectResult.Value as TrackedMovieToWatch;
                Assert.NotNull(tracked.Movie);
            }

            [Fact]
            public async Task Returns_OK_When_Tracked_Movie_Watched_Exists()
            {
                // Arrange
                // Add existing tracked Movie
                var (watched, movies) = await CreateWatched(1);

                // Act
                var result = await controller.GetTrackedMovie(watched.First().UserId, watched.First().Id);

                // Assert
                var okObjectResult = Assert.IsType<OkObjectResult>(result);
                var tracked = okObjectResult.Value as TrackedMovieWatched;
                Assert.NotNull(tracked.Movie);
                Assert.NotNull(tracked.WatchedHistory);
            }

            [Fact]
            public async Task Returns_Not_Found_When_Tracked_Movie_Does_Not_Exist()
            {
                // Act
                var result = await controller.GetTrackedMovie("NoUser", 1);

                // Assert
                Assert.IsType<NotFoundResult>(result);
            }
        }

        public class GetTrackedMoviesShould: TrackedMoviesControllerFacts
        {

            [Fact]
            public async Task Returns_OK_When_Tracked_Movies_Exist()
            {
                // arrange
                var (watchedMovies, movies) = await CreateWatched(3);

                // act
                var result = await controller.GetTrackedMovies(testUserId);

                // assert
                var okObjectResult = Assert.IsType<OkObjectResult>(result);
                var trackedMoviesReturned = Assert.IsType<List<TrackedMovie>>(okObjectResult.Value);
                trackedMoviesReturned.Should().HaveCount(3);

                Assert.True(trackedMoviesReturned.All(x => x.Movie != null));
                Assert.True(trackedMoviesReturned.OfType<TrackedMovieWatched>().All(x => x.WatchedHistory != null));
            }


            [Fact]
            public async Task Returns_OK_With_Empty_List_When_No_Tracked_Movies()
            {
                // arrange


                // act
                var result = await controller.GetTrackedMovies(testUserId);

                // assert
                var okObjectResult = Assert.IsType<OkObjectResult>(result);
                var trackedMoviesReturned = Assert.IsType<List<TrackedMovie>>(okObjectResult.Value);
                trackedMoviesReturned.Should().HaveCount(0);
            }
        }

        public class AddWatchedMovieShould: TrackedMoviesControllerFacts
        {
            [Fact]
            public async Task Returns_SeeOther_When_Tracked_Movie_Exists()
            {
                // arrange
                var (watchedMovies, movies) = await CreateWatched();
                var request = fixture.Create<AddWatchedMovieRequest>();
                request.Movie = movies.First();

                // act
                var result = await controller.AddWatchedMovie(testUserId, request);

                // assert
                var statusCodeResult = Assert.IsType<StatusCodeResult>(result);
                statusCodeResult.StatusCode.Should().Be((int)HttpStatusCode.SeeOther);
            }

            [Fact]
            public async Task Returns_Created_When_Tracked_Added_For_Non_Existing_Movie()
            {
                // arrange
                var request = fixture.Create<AddWatchedMovieRequest>();

                // act
                var result = await controller.AddWatchedMovie(testUserId, request);

                // assert
                var createdResult = Assert.IsType<CreatedResult>(result);
                createdResult.Location.Should().Be(fakeRoute);
            }

            [Fact]
            public async Task Returns_Created_When_Tracked_Added_For_Existing_Movie()
            {
                // arrange
                var movie = (await CreateMovies("testMovieId")).First();
                var request = fixture.Create<AddWatchedMovieRequest>();
                request.Movie = movie;

                // act
                var result = await controller.AddWatchedMovie(testUserId, request);

                // assert
                var createdResult = Assert.IsType<CreatedResult>(result);
                createdResult.Location.Should().Be(fakeRoute);
            }
        }

        public class AddMovieToWatchShould : TrackedMoviesControllerFacts
        {
            [Fact]
            public async Task Returns_SeeOther_When_Tracked_Movie_Exists()
            {
                // arrange
                var (toWatch, movies) = await CreateToWatch();

                // act
                var result = await controller.AddMovieToWatch(testUserId, movies.First());

                // assert
                var statusCodeResult = Assert.IsType<StatusCodeResult>(result);
                statusCodeResult.StatusCode.Should().Be((int)HttpStatusCode.SeeOther);
            }

            [Fact]
            public async Task Returns_Created_When_Tracked_Added_For_Non_Existing_Movie()
            {
                // arrange
                var movie = fixture.Create<Movie>();

                // act
                var result = await controller.AddMovieToWatch(testUserId, movie);

                // assert
                var createdResult = Assert.IsType<CreatedResult>(result);
                createdResult.Location.Should().Be(fakeRoute);
            }

            [Fact]
            public async Task Returns_Created_When_Tracked_Added_For_Existing_Movie()
            {
                // arrange
                var movie = (await CreateMovies("testMovieId")).First();

                // act
                var result = await controller.AddMovieToWatch(testUserId, movie);

                // assert
                var createdResult = Assert.IsType<CreatedResult>(result);
                createdResult.Location.Should().Be(fakeRoute);
            }
        }

        public class UpdateWatchedMovieShould: TrackedMoviesControllerFacts
        {
            [Fact]
            public async Task Returns_OK_When_Update_Successful()
            {
                // arrange
                var (tracked, movies) = await CreateWatched(1);
                var request = fixture.Create<UpdateWatchedMovieRequest>();
                request.id = tracked.First().Id;

                // act
                var result = await controller.UpdateWatchedMovie(testUserId, request);

                // assert
                Assert.IsType<OkResult>(result);
            }

            [Fact]
            public async Task Returns_Conflict_When_Tracked_Movie_Not_Found()
            {
                // arrange
                var request = fixture.Create<UpdateWatchedMovieRequest>();

                // act
                var result = await controller.UpdateWatchedMovie(testUserId, request);

                // assert
                var statusCodeResult = Assert.IsType<ObjectResult>(result);
                statusCodeResult.StatusCode.Should().Be((int)HttpStatusCode.Conflict);
            }
        }

        public class DeleteTrackedMovieMovieShould : TrackedMoviesControllerFacts
        {
            [Fact]
            public async Task Returns_OK_When_Delete_Successful()
            {
                // arrange
                var (tracked, movies) = await CreateWatched(1);

                // act
                var result = await controller.DeleteTrackedMovie(testUserId, tracked.First().Id);

                // assert
                Assert.IsType<OkResult>(result);
            }

            [Fact]
            public async Task Returns_Conflict_When_Tracked_Movie_Not_Found()
            {
                // arrange

                // act
                var result = await controller.DeleteTrackedMovie(testUserId, 1);

                // assert
                var statusCodeResult = Assert.IsType<ObjectResult>(result);
                statusCodeResult.StatusCode.Should().Be((int)HttpStatusCode.Conflict);
            }
        }
    }
}
