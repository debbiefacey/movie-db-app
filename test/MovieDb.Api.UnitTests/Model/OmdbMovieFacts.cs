﻿using MovieDb.Api.Model;
using Newtonsoft.Json;
using System.Linq;
using Xunit;

namespace MovieDb.Api.UnitTests.Model.OmdbMovieFacts
{
    public class OmdbMovieShould
    {
        [Theory]
        [InlineData("{\"Title\":\"Interstellar\",\"Year\":\"2014\",\"Rated\":\"PG-13\",\"Released\":\"07 Nov 2014\",\"Runtime\":\"169 min\",\"Genre\":\"Adventure, Drama, Sci-Fi\",\"Director\":\"Christopher Nolan\",\"Writer\":\"Jonathan Nolan, Christopher Nolan\",\"Actors\":\"Ellen Burstyn, Matthew McConaughey, Mackenzie Foy, John Lithgow\",\"Plot\":\"A team of explorers travel through a wormhole in space in an attempt to ensure humanity's survival.\",\"Language\":\"English\",\"Country\":\"USA, UK, Canada, Iceland\",\"Awards\":\"Won 1 Oscar. Another 41 wins & 142 nominations.\",\"Poster\":\"https://images-na.ssl-images-amazon.com/images/M/MV5BMjIxNTU4MzY4MF5BMl5BanBnXkFtZTgwMzM4ODI3MjE@._V1_SX300.jpg\",\"Ratings\":[{\"Source\":\"Internet Movie Database\",\"Value\":\"8.6/10\"},{\"Source\":\"Rotten Tomatoes\",\"Value\":\"71%\"},{\"Source\":\"Metacritic\",\"Value\":\"74/100\"}],\"Metascore\":\"74\",\"imdbRating\":\"8.6\",\"imdbVotes\":\"1,036,323\",\"imdbID\":\"tt0816692\",\"Type\":\"movie\",\"DVD\":\"31 Mar 2015\",\"BoxOffice\":\"$158,737,441.00\",\"Production\":\"Paramount Pictures\",\"Website\":\"http://www.InterstellarMovie.com\",\"Response\":\"True\"}")]
        [InlineData("{\"Title\":\"War for the Planet of the Apes\",\"Year\":\"2017\",\"Rated\":\"N/A\",\"Released\":\"14 Jul 2017\",\"Runtime\":\"N/A\",\"Genre\":\"Action, Adventure, Drama\",\"Director\":\"Matt Reeves\",\"Writer\":\"Mark Bomback, Pierre Boulle (novel), Matt Reeves\",\"Actors\":\"Judy Greer, Woody Harrelson, Andy Serkis, Steve Zahn\",\"Plot\":\"After the apes suffer unimaginable losses, Caesar wrestles with his darker instincts and begins his own mythic quest to avenge his kind.\",\"Language\":\"English\",\"Country\":\"USA\",\"Awards\":\"N/A\",\"Poster\":\"https://images-na.ssl-images-amazon.com/images/M/MV5BMjM2MTg2OTIwMV5BMl5BanBnXkFtZTgwODI1Njg4MTI@._V1_SX300.jpg\",\"Ratings\":[],\"Metascore\":\"N/A\",\"imdbRating\":\"N/A\",\"imdbVotes\":\"N/A\",\"imdbID\":\"tt3450958\",\"Type\":\"movie\",\"DVD\":\"N/A\",\"BoxOffice\":\"N/A\",\"Production\":\"20th Century Fox\",\"Website\":\"http://www.warfortheplanet.com/\",\"Response\":\"True\"}")]
        public void Be_Deserializable_From_Json_Returned_From_Omdb_Api(string json)
        {
            var omdbMovie = JsonConvert.DeserializeObject<OmdbMovie>(json);
            Assert.NotNull(omdbMovie);

            omdbMovie.Ratings.ForEach(r => { var num = r.NumberValue; });
        }
    }
}
