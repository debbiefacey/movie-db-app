// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import Vuex from 'vuex';
import Vuetify from 'vuetify';
import App from './App.vue';
import router from './router';
import storeOptions from './store';

Vue.config.productionTip = false;

Vue.use(Vuex);
const store = new Vuex.Store(storeOptions);

Vue.use(Vuetify);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App)
});
