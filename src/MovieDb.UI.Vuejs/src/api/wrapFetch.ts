const UNAUTHORIZED = 'You do not have permission to execute that request';
const TIMEOUT_ERROR = 'We can\'t connect to the server. Are you online?';
const UNHANDLED_ERROR = 'We got an unexpected error. Please try your request again';

export function wrapFetch() {

  function wrappedFetch(request: RequestInfo, init?: RequestInit): Promise<any> {

    const fetchRequest = (
      fetch(request)
      .then(response => _handleError(response))
      .catch(error => {
        console.dir(error);
        // Rethrow error to be caught in calling program
        return Promise.reject(UNHANDLED_ERROR);
      })
    );

    return _timeoutPromise(1000, fetchRequest);
  }

  function _handleError(response: Response) {
    if (response.status === 401) {
      throw new Error(UNAUTHORIZED);
    }
    if (!response.ok) {
      throw new Error(response.statusText);
    }
    return response;
  }

  function _timeoutPromise<T>(ms: number, promise: Promise<T>) {
    return new Promise((resolve, reject) => {
      const timeoutId = setTimeout(() => {
        reject(TIMEOUT_ERROR);
      }, ms);
      promise.then(
        (res) => {
          clearTimeout(timeoutId);
          resolve(res);
        },
        (err) => {
          clearTimeout(timeoutId);
          reject(err);
        }
      );
    });
  }

  return wrappedFetch;
}

export default wrapFetch();
