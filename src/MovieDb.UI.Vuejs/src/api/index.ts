import wrappedFetch from './wrapFetch';
import { ITrackedMovie } from './../model/ITrackedMovie';

const BASE_URL = 'https://localhost/';

export function fetchTrackedMovies(userId: string)
{
    return wrappedFetch(`${BASE_URL}${userId}/tracked-movies`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(response => response.json() as ITrackedMovie[]);
}

