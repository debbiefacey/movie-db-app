import Vue from 'vue';
import Router from 'vue-router';
import ToWatchPage from '@/pages/ToWatchPage.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'ToWatchPage',
      component: ToWatchPage,
    },
  ],
});
