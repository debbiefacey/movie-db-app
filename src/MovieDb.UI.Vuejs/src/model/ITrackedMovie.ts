import { IMovie } from './IMovie';
import { IWatchedDetail } from './IWatchedDetail';
import { TrackType } from './TrackType';

export interface ITrackedMovie {
  type: TrackType;

  id: number;
  userId: string;
  movieId: string;
  movie: IMovie;

  isFavorite?: boolean;
  lastRating?: number;
  lastWatchedDate?: Date;
  lastComments?: string;
  watchedHistory?: IWatchedDetail[];
}
