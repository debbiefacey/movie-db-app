interface IMovie {
  movieId: string;
  title: string;
  poster: string;
  year: number;
  trailer: string;
  tomatoMeter: number;
  imdbRating: number;
}

export { IMovie }
