interface IWatchedDetail
{
  id: number;
  userId: string;
  movieId: string;
  trackedMovieId: number;
  rating: number;
  watchedDate: Date;
  comments: string;
}

export { IWatchedDetail }
