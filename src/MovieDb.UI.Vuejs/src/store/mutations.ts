import { ITrackedMovie } from './../model/ITrackedMovie';
import { State } from './state';

const mutations = {
  setTrackedMovies(state: State, payload: {trackedMovies: ITrackedMovie[]}) {
    state.trackedMovies = payload.trackedMovies;
  }
};

type CommitPayload = {
  type: 'setTrackedMovies',
  trackedMovies: ITrackedMovie[]
}

export default mutations;
export { CommitPayload };
