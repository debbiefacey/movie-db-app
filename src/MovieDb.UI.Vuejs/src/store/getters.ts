import Vuex from 'vuex';
import Vue from 'vue';
import { State } from './state';
import { ITrackedMovie } from '../model/ITrackedMovie';

const getters = {
  moviesToWatch(state: State): ITrackedMovie[] {
    return state.trackedMovies.filter(m => m.type == 'ToWatch');
  },

  moviesWatched(state: State): ITrackedMovie[] {
    return state.trackedMovies.filter(m => m.type == 'Watched');
  }
};

type Getters = typeof getters;

export default getters;
export { Getters };
