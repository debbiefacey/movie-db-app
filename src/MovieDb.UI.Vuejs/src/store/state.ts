import { ITrackedMovie } from '../model/ITrackedMovie'

let state: State = {
  trackedMovies: [] as ITrackedMovie[]
};

interface State {
  trackedMovies: ITrackedMovie[],
}

export default state;
export { State };
