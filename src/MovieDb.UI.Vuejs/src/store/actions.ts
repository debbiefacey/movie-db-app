import { ITrackedMovie } from './../model/ITrackedMovie';
import { IActionContext } from './IActionContext';
import { fetchTrackedMovies } from '../api';

const actions = {
  loadMovies({commit}: IActionContext, {userId}: { userId: string}) {
    return fetchTrackedMovies(userId)
      .then(trackedMovies => commit({type: 'setTrackedMovies', trackedMovies }));
  }
}

type DispatchPayload = {
  type: 'loadMovies',
  userId: string
}

export default actions;
export { DispatchPayload }
