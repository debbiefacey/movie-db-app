import { ActionContext, DispatchOptions, CommitOptions } from 'vuex';
import { State } from './state';
import { Getters } from './getters';
import { DispatchPayload } from './actions';
import { CommitPayload } from './mutations';

interface Dispatch {
  (payloadWithType: DispatchPayload, options?: DispatchOptions): Promise<any[]>;
}

interface Commit {
  (payloadWithType: CommitPayload, options?: CommitOptions): void;
}

export interface IActionContext {
  state: State;
  dispatch: Dispatch;
  commit: Commit;
  getters: Getters;
}
