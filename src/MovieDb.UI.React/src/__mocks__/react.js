global.window = global
window.addEventListener = () => {}
window.requestAnimationFrame = () => {
  throw new Error('requestAnimationFrame is not supported in Node')
}

// https://github.com/facebook/react/issues/9102#issuecomment-283873039
