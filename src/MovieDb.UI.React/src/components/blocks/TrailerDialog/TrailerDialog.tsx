import * as React from 'react'
import Dialog, {
  DialogProps,
  DialogContent,
  DialogActions,
} from 'material-ui/Dialog'
import ResponsiveEmbed from 'react-responsive-embed'

import Button from '../../elements/Button'

interface OwnProps {
  videoKey: string
  width?: number
  onClose?: (event: any) => void
}

type TrailerDialogProps = OwnProps & DialogProps

const TrailerDialog = ({
  videoKey,
  width,
  onClose,
  ...muiProps
}: TrailerDialogProps) => {
  return (
    <Dialog {...muiProps}>
      <DialogContent>
        <div style={{ width }}>
          <ResponsiveEmbed
            src={'https://www.youtube.com/embed/' + videoKey}
            allowfullscreen
          />
        </div>
      </DialogContent>
      <DialogActions>
        <Button color="primary" autoFocus onClick={onClose}>
          Close
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export { TrailerDialog as default, TrailerDialogProps }
