import * as React from 'react'

import { storiesOf } from '@storybook/react'
import { withKnobs, number } from '@storybook/addon-knobs/react'

import TrailerDialog from './TrailerDialog'

storiesOf('TrailerDialog', module)
  .addDecorator(withKnobs)
  .add('open', () => (
    <TrailerDialog videoKey="1Q8fG0TtVAY" width={number('Width', 500)} open />
  ))
