import * as React from 'react'
import { CardActions } from 'material-ui/Card'
import { StyleRules } from 'material-ui/styles'
import { withStyles } from 'material-ui'

import Button from '../../elements/Button'

interface HocAddedProps {
  classes?: {
    cardActions: string
    flexGrow: string
  }
}

export interface MovieCardActionsProps {
  onToWatchClicked?: () => void
  onWatchedClicked?: () => void
  onTrailerClicked?: () => void
}

export enum MovieCardMode {
  Watched,
  ToWatch,
  Untracked,
}

function MovieCardActions({
  classes,
  onToWatchClicked,
  onWatchedClicked,
  onTrailerClicked,
  ...muiProps
}: MovieCardActionsProps & HocAddedProps) {
  return (
    <CardActions
      disableActionSpacing
      className={classes && classes.cardActions}
      {...muiProps}>
      <Button
        small
        icon="add"
        color="primary"
        style={{ marginRight: 3 }}
        onClick={onToWatchClicked}>
        To Watch
      </Button>
      <Button small icon="check" color="secondary" onClick={onWatchedClicked}>
        Watched
      </Button>
      <Button
        small
        icon="movie"
        color="secondary"
        style={{ fontSize: '1.0rem' }}
        onClick={onTrailerClicked}
      />
      <div className={classes && classes.flexGrow} />
    </CardActions>
  )
}

const styles: StyleRules = {
  cardActions: {
    height: 20,
    padding: '8px 4px',
  },
  flexGrow: {
    flex: '1 1 auto',
  },
}

export default withStyles(styles)(MovieCardActions)
