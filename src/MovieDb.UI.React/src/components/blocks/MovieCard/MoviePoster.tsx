import * as React from 'react'
import { CardMedia } from 'material-ui/Card'
import IconButton from 'material-ui/IconButton'
import FavoriteIcon from 'material-ui-icons/Favorite'
import FavoriteBorderIcon from 'material-ui-icons/FavoriteBorder'
import { withStyles, StyleRules } from 'material-ui/styles'
import { Relative, Absolute } from 'rebass'

interface HocAddedProps {
  classes?: {
    poster: string
    favoritesButton: string
  }
}

export interface MoviePosterProps {
  poster: string
  title: string
  favoriteSelectable?: boolean
  favorite?: boolean
  onFavoriteChanged?: (event: any) => void
}

export function MoviePoster({
  poster,
  title,
  favoriteSelectable,
  favorite,
  onFavoriteChanged,
  classes,
}: MoviePosterProps & HocAddedProps) {
  return (
    <Relative>
      <CardMedia
        className={classes && classes.poster}
        image={poster}
        title={title}>
        {favoriteSelectable ? (
          <Absolute top right>
            <IconButton
              aria-label="Add to favorites"
              className={classes && classes.favoritesButton}
              onClick={onFavoriteChanged}>
              {favorite ? (
                <FavoriteIcon color="secondary" />
              ) : (
                <FavoriteBorderIcon color="secondary" />
              )}
            </IconButton>
          </Absolute>
        ) : null}
      </CardMedia>
    </Relative>
  )
}

const styles: StyleRules = {
  poster: {
    height: 223,
  },
}

export default withStyles(styles)(MoviePoster)
