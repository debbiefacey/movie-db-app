import * as React from 'react'
import Card, { CardContent } from 'material-ui/Card'
import Typography from 'material-ui/Typography'
import { withStyles, StyleRules } from 'material-ui/styles'

import MoviePoster, { MoviePosterProps } from './MoviePoster'
import MovieCardActions, { MovieCardActionsProps } from './MovieCardActions'

interface HocAddedProps {
  classes?: {
    card: string
    titleContent: string
    titleText: string
  }
}

type PosterAndCardActionProps = MoviePosterProps & MovieCardActionsProps

export interface MovieCardProps extends PosterAndCardActionProps {
  year: number
}

export function MovieCard({
  poster,
  title,
  year,
  favoriteSelectable,
  favorite,
  onFavoriteChanged,
  onToWatchClicked,
  onWatchedClicked,
  onTrailerClicked,
  classes,
}: MovieCardProps & HocAddedProps) {
  return (
    <Card className={classes && classes.card}>
      <MovieCardActions
        onToWatchClicked={onToWatchClicked}
        onWatchedClicked={onWatchedClicked}
        onTrailerClicked={onTrailerClicked}
      />
      <MoviePoster
        poster={poster}
        title={title}
        favoriteSelectable={favoriteSelectable}
        favorite={favorite}
        onFavoriteChanged={onFavoriteChanged}
      />
      <CardContent className={classes && classes.titleContent}>
        <Typography component="p" className={classes && classes.titleText}>
          {title} ({year})
        </Typography>
      </CardContent>
    </Card>
  )
}

const styles: StyleRules = {
  card: {
    maxWidth: 150,
  },
  titleContent: {
    padding: 4,
    '&:last-child': {
      padding: 4,
    },
  },
  titleText: {
    fontSize: '.7rem',
    fontWeight: 500,
    padding: 2,
  },
}

export default withStyles(styles)(MovieCard)
