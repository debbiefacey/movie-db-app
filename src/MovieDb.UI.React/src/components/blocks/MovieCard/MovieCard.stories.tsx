import * as React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'

import MovieCard from './MovieCard'

storiesOf('MovieCard', module)
  .add('default', () => (
    <MovieCard
      // tslint:disable-next-line:max-line-length
      poster="https://images-na.ssl-images-amazon.com/images/M/MV5BMjMyNDkzMzI1OF5BMl5BanBnXkFtZTgwODcxODg5MjI@._V1_SX300.jpg"
      title="Thor: Ragnarok"
      year={2017}
      onToWatchClicked={action('To Watch')}
      onWatchedClicked={action('Watched')}
      onTrailerClicked={action('Trailer')}
    />
  ))
  .add('favorite not selected', () => (
    <MovieCard
      // tslint:disable-next-line:max-line-length
      poster="https://images-na.ssl-images-amazon.com/images/M/MV5BMjMyNDkzMzI1OF5BMl5BanBnXkFtZTgwODcxODg5MjI@._V1_SX300.jpg"
      title="Thor: Ragnarok"
      year={2017}
      favoriteSelectable
      onFavoriteChanged={action('onFavoriteChanged')}
      onToWatchClicked={action('To Watch')}
      onWatchedClicked={action('Watched')}
      onTrailerClicked={action('Trailer')}
    />
  ))
  .add('favorite selected', () => (
    <MovieCard
      // tslint:disable-next-line:max-line-length
      poster="https://images-na.ssl-images-amazon.com/images/M/MV5BMjMyNDkzMzI1OF5BMl5BanBnXkFtZTgwODcxODg5MjI@._V1_SX300.jpg"
      title="Thor: Ragnarok"
      year={2017}
      favoriteSelectable
      onFavoriteChanged={action('onFavoriteChanged')}
      favorite
      onToWatchClicked={action('To Watch')}
      onWatchedClicked={action('Watched')}
      onTrailerClicked={action('Trailer')}
    />
  ))
