import * as React from 'react'

import MuiButton, { ButtonProps as MuiButtonProps } from 'material-ui/Button'
import Icon from 'material-ui/Icon'
import { StyleRules } from 'material-ui/styles'
import withStyles from 'material-ui/styles/withStyles'

export interface ButtonProps extends MuiButtonProps {
  children?: React.ReactNode
  small?: boolean
  icon?: string
  classes?: any
}

export function Button({
  children,
  small,
  icon,
  classes,
  ...muiProps
}: ButtonProps) {
  return (
    <MuiButton {...muiProps} className={small && classes && classes.small}>
      {icon ? <Icon className={classes && classes.icon}>{icon}</Icon> : null}
      {children}
    </MuiButton>
  )
}

const styles: StyleRules = {
  small: {
    padding: '3px 4px',
    minHeight: 15,
    minWidth: 0,
    fontSize: '0.5rem',
  },
  icon: {
    fontSize: 'inherit',
  },
}

export default withStyles(styles)(Button)
