import * as React from 'react'

import { storiesOf } from '@storybook/react'

import Button from './Button'

storiesOf('Button', module)
  .add('default', () => <Button>Hello</Button>)
  .add('with MUI Props', () => (
    <Button variant="raised" color="primary">
      Hello
    </Button>
  ))
  .add('small', () => (
    <Button small variant="raised" color="primary">
      Hello
    </Button>
  ))
  .add('small with icon', () => (
    <Button small icon="add" variant="raised" color="primary">
      Hello
    </Button>
  ))
  .add('flat and small with icon', () => (
    <Button variant="flat" small icon="add" color="primary">
      Hello
    </Button>
  ))
