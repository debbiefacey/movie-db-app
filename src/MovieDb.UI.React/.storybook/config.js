import { configure } from '@storybook/react'

function loadStories() {
  require('../src/components/elements/Button/Button.stories')
  require('../src/components/blocks/TrailerDialog/TrailerDialog.stories')
  require('../src/components/blocks/MovieCard/MovieCard.stories')
}

configure(loadStories, module)
