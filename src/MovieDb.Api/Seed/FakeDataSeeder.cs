﻿using Microsoft.EntityFrameworkCore;
using MovieDb.Api.Service;
using MovieDb.Data;
using MovieDb.Data.Model;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace MovieDb.Api.Seed
{
    /// <summary>
    /// See fake data in the database
    /// </summary>
    public class FakeDataSeeder
    {
        private readonly MovieDbContext db;
        private readonly IOmdbApiService omdbApi;

        /// <summary>
        /// Initializes a new instance of the <see cref="FakeDataSeeder" /> class.
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="omdbApi">The omdb API.</param>
        public FakeDataSeeder(MovieDbContext db, IOmdbApiService omdbApi)
        {
            this.db = db;
            this.omdbApi = omdbApi;
        }

        /// <summary>
        /// Seeds the data.
        /// </summary>
        public async Task SeedData()
        {
            if(await db.TrackedMovies.AnyAsync())
            {
                return;
            }

            var faker = new Bogus.Faker();
            var userId = faker.Person.UserName;

            var watchedMovies = new[]
            {
                omdbApi.GetMovieByTitleAndYear("Gifted", "2017"),
                omdbApi.GetMovieByTitleAndYear("Interstellar", "2014"),
                omdbApi.GetMovieByTitleAndYear("The Dark Knight", "2008"),
                omdbApi.GetMovieByTitleAndYear("Rise of the Planet of the Apes", "2011"),
                omdbApi.GetMovieByTitleAndYear("Alien", "1979"),
                omdbApi.GetMovieByTitleAndYear("The Sound of Music", "1965"),
                omdbApi.GetMovieByTitleAndYear("I Can't Think Straight", "2008"),
                omdbApi.GetMovieByTitleAndYear("Kiss Me", "2011"),
                omdbApi.GetMovieByTitleAndYear("The Hunger Games", "2012"),
                omdbApi.GetMovieByTitleAndYear("The Hunger Games: Catching Fire", "2013"),
                omdbApi.GetMovieByTitleAndYear("The Hunger Games: Mockingjay - Part 1", "2014"),
                omdbApi.GetMovieByTitleAndYear("The Hunger Games: Mockingjay - Part 2", "2015")
            };

            var toWatchedMovies = new[]
            {
                omdbApi.GetMovieByTitleAndYear("War for the Planet of the Apes", "2017"),
                omdbApi.GetMovieByTitleAndYear("Get Out", "2017"),
                omdbApi.GetMovieByTitleAndYear("Spider-Man: Homecoming", "2017"),
                omdbApi.GetMovieByTitleAndYear("Guardians of the Galaxy Vol. 2", "2017")
            };

            var watchedMovieResults = await Task.WhenAll(watchedMovies);
            foreach (var (movie, statusCode, _) in watchedMovieResults)
            {
                if(statusCode == HttpStatusCode.OK)
                {
                    db.Movies.Add(new Movie
                    {
                        MovieId = movie.ImdbID,
                        ImdbRating = movie.Ratings.SingleOrDefault(x => x.Source == "Internet Movie Database")?.NumberValue ?? 0,
                        TomatoMeter = movie.Ratings.SingleOrDefault(x => x.Source == "Rotten Tomatoes")?.NumberValue ?? 0,
                        Poster = movie.Poster,
                        Title = movie.Title,
                        Year = movie.Year ?? 0
                    });

                    db.TrackedMovies.Add(new TrackedMovieWatched
                    {
                        IsFavorite = faker.PickRandom(new[] { true, false}),
                        MovieId = movie.ImdbID,
                        UserId = userId,
                        WatchedHistory = new[]
                        {
                            new WatchedDetail
                            {
                                Comments = faker.Rant.Review(movie.Title),
                                MovieId = movie.ImdbID,
                                Rating = faker.Random.Byte(60, 100),
                                UserId = userId,
                                WatchedDate = faker.Date.Between(movie.Released ?? new DateTime(movie.Year ?? DateTime.Today.Year, 1, 1), DateTime.Today)
                            }
                        }
                    });
                }
            }

            var toWatchMovieResults = await Task.WhenAll(toWatchedMovies);
            foreach (var (movie, statusCode, _) in toWatchMovieResults)
            {
                if (statusCode == HttpStatusCode.OK)
                {
                    db.Movies.Add(new Movie
                    {
                        MovieId = movie.ImdbID,
                        ImdbRating = movie.Ratings.SingleOrDefault(x => x.Source == "Internet Movie Database")?.NumberValue ?? 0,
                        TomatoMeter = movie.Ratings.SingleOrDefault(x => x.Source == "Rotten Tomatoes")?.NumberValue ?? 0,
                        Poster = movie.Poster,
                        Title = movie.Title,
                        Year = movie.Year ?? 0
                    });

                    db.TrackedMovies.Add(new TrackedMovieToWatch
                    {
                        MovieId = movie.ImdbID,
                        UserId = userId
                    });
                }
            }

            await db.SaveChangesAsync();
        }
    }
}
