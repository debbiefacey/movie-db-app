﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieDb.Api.Model;
using MovieDb.Data;
using MovieDb.Data.Model;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace MovieDb.Api.Controllers
{
    /// <summary>
    /// TrackedMovies
    /// </summary>
    /// <seealso cref="Controller" />
    [Route("tracked-movies")]
    public class TrackedMoviesController: Controller
    {
        private readonly MovieDbContext db;

        /// <summary>
        /// Initializes a new instance of the <see cref="TrackedMoviesController"/> class.
        /// </summary>
        /// <param name="db">The database.</param>
        public TrackedMoviesController(MovieDbContext db)
        {
            this.db = db;
        }

        /// <summary>
        /// Gets the tracked movie.
        /// </summary>       
        /// <param name="id">Thetracking identifier.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}", Name = "GetTrackedMovie")]
        [ProducesResponseType(typeof(TrackedMovieWatched), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> GetTrackedMovie([FromRoute]int id)
        {
            // TODO: Test is userId is logged in user

            var trackedMovie =
                await db.TrackedMovies
                .Include(x => x.Movie)
                .SingleOrDefaultAsync(x => x.Id == id);

            var trackedMovieWatched = trackedMovie as TrackedMovieWatched;
            if(trackedMovieWatched != null)
            {
                await db.Entry(trackedMovieWatched).Collection(x => x.WatchedHistory).LoadAsync();
            }

            if(trackedMovie == null)
            {
                return NotFound();
            }

            return Ok(trackedMovie);
        }

        /// <summary>
        /// Gets the tracked movies.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="title">The title.</param>
        /// <param name="ids">The movie ids.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        [ProducesResponseType(typeof(TrackedMovie), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetTrackedMovies([FromQuery]string userId, [FromQuery]string title, [FromQuery] string[] ids)
        {
            var trackedMovies =
                await db.TrackedMovies
                .Include(x => x.Movie)
                .Where(x => 
                    x.UserId == userId && 
                    (title == null || x.Movie.Title.ToLower().StartsWith(title.ToLower())) &&
                    (ids == null || ids.Contains(x.Movie.MovieId))
                 )
                .ToListAsync();

            await db.WatchedHistory
                .Where(x => x.UserId == userId)
                .LoadAsync();

            return Ok(trackedMovies);
        }

        /// <summary>
        /// Adds a watched movie to be tracked
        /// </summary>        
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("watched")]
        [ProducesResponseType(typeof(TrackedMovieWatched), (int)HttpStatusCode.Created)]        
        public async Task<IActionResult> AddWatchedMovie([FromBody]AddWatchedMovieRequest request)
        {
            await EnsureMovieInDB(db, request.Movie);

            var checkResult = await TrackedMovieExists(db, request.Movie.MovieId, request.UserId, TrackType.Watched);

            if(checkResult.exists)
            {
                return checkResult.actionResult;
            }

            var trackedMovie = new TrackedMovieWatched
            {
                MovieId = request.Movie.MovieId,
                UserId = request.UserId,
                IsFavorite = request.IsFavorite
            };

            trackedMovie.WatchedHistory.Add(new WatchedDetail
            {
                UserId = request.UserId,
                Rating = request.Rating,
                WatchedDate = request.WatchedDate,
                Comments = request.Comments
            });

            db.TrackedMovies.Add(trackedMovie);

            await db.SaveChangesAsync();

            trackedMovie.Movie = request.Movie;

            var createdUrl = Url.RouteUrl("GetTrackedMovie", new {               
                id = trackedMovie.Id
            });

            return Created(createdUrl, trackedMovie);
        }

        /// <summary>
        /// Adds a movie to be watched
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("to-watch")]
        [ProducesResponseType(typeof(TrackedMovieToWatch), (int)HttpStatusCode.Created)]        
        public async Task<IActionResult> AddMovieToWatch([FromBody]AddMovieToWatchRequest request)
        {
            await EnsureMovieInDB(db, request.Movie);

            var checkResult = await TrackedMovieExists(db, request.Movie.MovieId, request.UserId, TrackType.ToWatch);

            if (checkResult.exists)
            {
                return checkResult.actionResult;
            }

            var trackedMovie = new TrackedMovieToWatch
            {
                MovieId = request.Movie.MovieId,
                UserId = request.UserId
            };

            db.TrackedMovies.Add(trackedMovie);

            await db.SaveChangesAsync();

            trackedMovie.Movie = request.Movie;

            var createdUrl = Url.RouteUrl("GetTrackedMovie", new
            {               
                id = trackedMovie.Id
            });

            return Created(createdUrl, trackedMovie);
        }

        /// <summary>
        /// Updates the watched movie.
        /// </summary>       
        /// <param name="request">The request.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        [HttpPut]
        [Route("{id}/watched")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.Conflict)]
        public async Task<IActionResult> UpdateWatchedMovie([FromBody]UpdateWatchedMovieRequest request)
        {
            var watchedMovie = db.TrackedMovies
                .OfType<TrackedMovieWatched>()
                .SingleOrDefault(t => t.Id == request.id && t.UserId == request.UserId);

            if(watchedMovie == null)
            {
                return StatusCode((int)HttpStatusCode.Conflict, "A suitable tracked movie to update could not be found");
            }

            watchedMovie.IsFavorite = request.IsFavorite;
            watchedMovie.WatchedHistory.Add(new WatchedDetail
            {
                Comments = request.Comments,
                MovieId = watchedMovie.MovieId,
                Rating = request.Rating,
                UserId = request.UserId,
                WatchedDate = request.WatchedDate
            });
            await db.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Remove tracking of movie
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="id">The tracking id</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.Conflict)]
        public async Task<IActionResult> DeleteTrackedMovie([FromRoute]string userId, [FromRoute]int id)
        {
            var trackedMovie = db.TrackedMovies
                .SingleOrDefault(t => t.Id == id && t.UserId == userId);

            if (trackedMovie == null)
            {
                return StatusCode((int)HttpStatusCode.Conflict, "A suitable tracked movie to delete could not be found");
            }

            db.TrackedMovies.Remove(trackedMovie);
            await db.SaveChangesAsync();

            return Ok();
        }

        private async Task EnsureMovieInDB(MovieDbContext db, Movie movie)
        {
            if (!await db.Movies.AnyAsync(m => m.MovieId == movie.MovieId))
            {
                db.Movies.Add(movie);
            }

        }

        private async Task<(bool exists, IActionResult actionResult)> TrackedMovieExists(MovieDbContext db, string movieId, string userId, TrackType trackType)
        {
            // Check if movie is already tracked
            var query = db.TrackedMovies
                .Where(t => t.MovieId == movieId && t.UserId == userId);

            query = trackType == TrackType.ToWatch ?
                (IQueryable<TrackedMovie>)query.OfType<TrackedMovieToWatch>() :
                query.OfType<TrackedMovieWatched>();

            var trackedMovieId =
                await query
                .Select(t => (int?)t.Id)
                .SingleOrDefaultAsync();

            // If movie is tracked return SeeOther status with location of existing tracked movie
            if (trackedMovieId.HasValue)
            {
                var url = Url.RouteUrl("GetTrackedMovie", new
                {
                    userId = userId,
                    id = trackedMovieId
                });
                Response.Headers.Add("Location", url);

               return(true, StatusCode((int)HttpStatusCode.SeeOther));
            }

            return (false, null);
        }
    }
}
