﻿using Microsoft.AspNetCore.Mvc;
using MovieDb.Api.Model;
using MovieDb.Api.Service;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace MovieDb.Api.Controllers
{
    /// <summary>
    /// The Movie Controller
    /// </summary>
    /// <seealso cref="Controller" />
    [Route("movies")]
    public class MoviesContoller: Controller
    {
        private readonly IOmdbApiService omdbApi;

        /// <summary>
        /// Initializes a new instance of the <see cref="MoviesContoller"/> class.
        /// </summary>
        /// <param name="omdbApi">The omdb API.</param>
        public MoviesContoller(IOmdbApiService omdbApi)
        {
            this.omdbApi = omdbApi;
        }

        /// <summary>
        /// Gets the movie.
        /// </summary>
        /// <param name="movieId">The movie identifier.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{movieId}")]
        [ProducesResponseType(typeof(OmdbMovie), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        public Task<IActionResult> GetMovie([FromRoute]string movieId)
        {
            return GetMovieResult(() => omdbApi.GetMovieById(movieId));
        }

        /// <summary>
        /// Searches the movie.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="year">The year.</param>
        /// <param name="page">The page.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        [ProducesResponseType(typeof(OmdbMovieSearchResult), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        public Task<IActionResult> SearchMovie([FromQuery]string title, [FromQuery]string year = null, [FromQuery]int? page = 1)
        {
            return GetMovieResult(() => omdbApi.SearchMovie(title, year, page.Value));
        }

        private async Task<IActionResult> GetMovieResult<T>(
            Func<Task<(T movie, HttpStatusCode statusCode, string error)>> getMovieFromApi)
        {
            var (movie, statusCode, error) = await getMovieFromApi();

            if (error != null)
            {
                return StatusCode((int)statusCode, error);
            }

            return Ok(movie);
        }
    }
}
