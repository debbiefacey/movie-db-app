﻿using Newtonsoft.Json;
using System;
using System.Globalization;

namespace MovieDb.Api.Json
{
    /// <summary>
    /// Omdb Movie API constants
    /// </summary>
    public static class OmdbConstants
    {
        /// <summary>
        /// The N/A string
        /// </summary>
        public const string NA = "N/A";
    }

    /// <summary>
    /// Converter for Omdb movie runtime
    /// </summary>
    /// <seealso cref="JsonConverter" />
    public class OmdbMovieRuntmeConverter : JsonConverter
    {
        /// <inheritdoc/>
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(short?);
        }

        /// <inheritdoc/>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            string value = serializer.Deserialize<string>(reader);

            if (value == OmdbConstants.NA)
            {
                return null;
            }

            return short.Parse(value.Replace(" min", ""));
        }

        /// <inheritdoc/>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value);
        }
    }

    /// <summary>
    /// Parse decimal values
    /// </summary>
    /// <seealso cref="JsonConverter" />
    public class OmdbDecimalJsonConverter : JsonConverter
    {
        /// <inhertidoc/>
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(decimal?);
        }

        /// <inhertidoc/>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var value = serializer.Deserialize<string>(reader);

            if(value == OmdbConstants.NA)
            {
                return null;
            }

            return decimal.Parse(
                value,
                NumberStyles.Currency,
                CultureInfo.CurrentCulture);
        }

        /// <inhertidoc/>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value);
        }
    }

    /// <summary>
    /// DateTime converter for date formats returned by Omdb API
    /// </summary>
    public class OmdbDatetimeJsonConverter : JsonConverter
    {
        private string dateTimeFormat = "dd MMM yyyy";

        /// <inheritdoc/>
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTime?);
        }

        /// <inheritdoc/>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var value = serializer.Deserialize<string>(reader);

            if (value == OmdbConstants.NA)
            {
                return null;
            }

            return DateTime.ParseExact(value, dateTimeFormat, CultureInfo.CurrentCulture);
        }

        /// <inheritdoc/>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value);
        }
    }

    /// <summary>
    /// Number converter for date formats returned by Omdb API
    /// </summary>
    public class OmdbNumberJsonConverter : JsonConverter
    {
        /// <inheritdoc/>
        public override bool CanConvert(Type objectType)
        {
            return
                objectType == typeof(byte?) ||
                objectType == typeof(short?) ||
                objectType == typeof(int?) ||
                objectType == typeof(long?) ||
                objectType == typeof(float?) ||
                objectType == typeof(double?);
        }

        /// <inheritdoc/>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var value = serializer.Deserialize<string>(reader);

            if (value == OmdbConstants.NA)
            {
                return null;
            }

            value = value.Replace(",", "");

            if(objectType == typeof(byte?))
            {
                return byte.Parse(value);
            }

            if (objectType == typeof(short?))
            {
                return short.Parse(value);
            }

            if (objectType == typeof(int?))
            {
                return int.Parse(value);
            }

            if (objectType == typeof(long?))
            {
                return long.Parse(value);
            }

            if (objectType == typeof(float?))
            {
                return float.Parse(value);
            }

            if (objectType == typeof(double?))
            {
                return double.Parse(value);
            }

            return null;
        }

        /// <inheritdoc/>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value);
        }
    }
}
