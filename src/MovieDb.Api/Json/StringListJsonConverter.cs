﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieDb.Api.Json
{
    /// <summary>
    /// Converts CSV list of an IEnumerable of strings
    /// </summary>
    /// <seealso cref="Newtonsoft.Json.JsonConverter" />
    public class StringListJsonConverter : JsonConverter
    {
        /// <inheritdoc />
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(IEnumerable<string>);
        }

        /// <inheritdoc />
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var csv = serializer.Deserialize<string>(reader);
            return csv?.Split(new[] { ", " }, StringSplitOptions.RemoveEmptyEntries);
        }

        /// <inheritdoc />
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value);
        }
    }
}
