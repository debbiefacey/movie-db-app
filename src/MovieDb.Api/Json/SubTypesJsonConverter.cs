﻿using Humanizer;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace MovieDb.Api.Json
{
    /// <summary>
    /// Converter for a class' sub types
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <seealso cref="Newtonsoft.Json.JsonConverter" />
    public abstract class SubTypesJsonConverter<T> : JsonConverter
    {
        /// <summary>
        /// Gets or sets the name of the type property.
        /// </summary>
        /// <value>
        /// The name of the type property.
        /// </value>
        public string TypePropertyName { get; protected set; } = "type";

        /// <summary>
        /// Determines whether an instance of the current System.Type can be assigned from an instance of the specified Type.
        /// </summary>
        /// <param name="objectType">The object type</param>
        /// <returns></returns>
        public override bool CanConvert(Type objectType)
        {
            return typeof(T).IsAssignableFrom(objectType);
        }

        /// <summary>
        /// Reads JSON and returns the appropriate object
        /// </summary>
        /// <param name="reader">The JsonReader</param>
        /// <param name="objectType">The object type</param>
        /// <param name="existingValue">The existing object value</param>
        /// <param name="serializer">The JsonSerializer</param>
        /// <returns></returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            // load the json string
            var jsonObject = JObject.Load(reader);

            // instantiate the appropriate object based on the json string
            var target = Create(objectType, jsonObject);

            JsonReader jsonObjectReader = jsonObject.CreateReader();
            jsonObjectReader.Culture = reader.Culture;
            jsonObjectReader.DateFormatString = reader.DateFormatString;
            jsonObjectReader.DateParseHandling = reader.DateParseHandling;
            jsonObjectReader.DateTimeZoneHandling = reader.DateTimeZoneHandling;
            jsonObjectReader.FloatParseHandling = reader.FloatParseHandling;
            jsonObjectReader.MaxDepth = reader.MaxDepth;
            jsonObjectReader.SupportMultipleContent = reader.SupportMultipleContent;

            // populate the properties of the object
            serializer.Populate(jsonObjectReader, target);

            // return the object
            return target;
        }

        /// <summary>
        /// Creates the JSON based on the object passed in
        /// </summary>
        /// <param name="writer">The JsonWriter</param>
        /// <param name="value">The object value</param>
        /// <param name="serializer">The JsonSerializer</param>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            JObject jo = new JObject();
            Type type = value.GetType();
            jo.Add(TypePropertyName, GetTypeString(type));

            foreach (PropertyInfo prop in type.GetProperties())
            {
                if (prop.CanRead)
                {
                    object propVal = prop.GetValue(value, null);
                    if (propVal != null)
                    {
                        jo.Add(prop.Name.Camelize(), JToken.FromObject(propVal, serializer));
                    }
                }
            }

            jo.WriteTo(writer);
        }

        /// <summary>
        /// Gets the type string from the object type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        protected virtual string GetTypeString(Type type)
        {
            return type.Name;
        }

        /// <summary>
        /// Abstract method which implements the appropriate create method
        /// </summary>
        /// <param name="objectType">The object type</param>
        /// <param name="jsonObject">The JObject instance</param>
        /// <returns></returns>
        protected abstract T Create(Type objectType, JObject jsonObject);
    }
}
