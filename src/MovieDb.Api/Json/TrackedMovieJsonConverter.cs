﻿using MovieDb.Data.Model;
using System;
using Newtonsoft.Json.Linq;

namespace MovieDb.Api.Json
{
    /// <summary>
    /// Json converter for TrackedMovie
    /// </summary>
    /// <seealso cref="SubTypesJsonConverter{TrackedMovie}" />
    public class TrackedMovieJsonConverter : SubTypesJsonConverter<TrackedMovie>
    {
        /// <summary>
        /// Abstract method which implements the appropriate create method
        /// </summary>
        /// <param name="objectType">The object type</param>
        /// <param name="jsonObject">The JObject instance</param>
        /// <returns></returns>
        protected override TrackedMovie Create(Type objectType, JObject jsonObject)
        {
            // examine the $type value
            string typeName = jsonObject[TypePropertyName].ToString();

            // based on the $type, instantiate and return a new object
            switch (typeName)
            {
                case "ToWatch":
                    return new TrackedMovieToWatch();
                case "Watched":
                    return new TrackedMovieWatched();

                default:
                    return null;
            }
        }

        /// <summary>
        /// Gets the type string from the object type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        protected override string GetTypeString(Type type)
        {
            return typeof(TrackedMovieToWatch) == type ? "ToWatch" : "Watched";
        }
    }
}
