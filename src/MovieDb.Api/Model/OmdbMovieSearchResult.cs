﻿using System;

namespace MovieDb.Api.Model
{
    /// <summary>
    /// Omdb movie search result
    /// </summary>
    public class OmdbMovieSearchResult
    {
        /// <summary>
        /// Gets or sets the movie list.
        /// </summary>
        /// <value>
        /// The movie list.
        /// </value>
        public OmdbMovieSummary[] Search { get; set; }

        /// <summary>
        /// Gets or sets the total results.
        /// </summary>
        /// <value>
        /// The total results.
        /// </value>
        public int TotalResults { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the response is successful.
        /// </summary>
        /// <value>
        ///   <c>true</c> if response is successful; otherwise, <c>false</c>.
        /// </value>
        public bool Response { get; set; }

        /// <summary>
        /// Gets or sets the current page number.
        /// </summary>
        /// <value>
        /// The current page number.
        /// </value>
        public int Page { get; set; }

        /// <summary>
        /// Gets the number of pages.
        /// </summary>
        /// <value>
        /// The number of pages.
        /// </value>
        public double NumberOfPages => Math.Ceiling((double)TotalResults / 10);
     }
}
