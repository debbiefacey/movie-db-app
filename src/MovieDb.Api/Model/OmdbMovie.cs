﻿using MovieDb.Api.Json;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace MovieDb.Api.Model
{
    /// <summary>
    /// Movie details from the Omdb API
    /// </summary>
    public class OmdbMovie
    {
        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the year.
        /// </summary>
        /// <value>
        /// The year.
        /// </value>
        [JsonConverter(typeof(OmdbNumberJsonConverter))]
        public short? Year { get; set; }

        /// <summary>
        /// Gets or sets the MPAA rating.
        /// </summary>
        /// <value>
        /// The rated.
        /// </value>
        public string Rated { get; set; }

        /// <summary>
        /// Gets or sets the released date.
        /// </summary>
        /// <value>
        /// The released.
        /// </value>
        [JsonConverter(typeof(OmdbDatetimeJsonConverter))]
        public DateTime? Released { get; set; }

        /// <summary>
        /// Gets a value indicating whether the movie was released this year.
        /// </summary>
        /// <value>
        ///   <c>true</c> if the movie was released this year; otherwise, <c>false</c>.
        /// </value>
        public bool ReleasedThisYear => Released.HasValue ? DateTime.Today.Year == Released.Value.Year : false;

        /// <summary>
        /// Gets a value indicating whether the movie was released last year.
        /// </summary>
        /// <value>
        ///   <c>true</c> if the movie was released this year; otherwise, <c>false</c>.
        /// </value>
        public bool ReleasedLastYear => Released.HasValue ? DateTime.Today.Year - 1 == Released.Value.Year : false;

        /// <summary>
        /// Gets or sets the runtime.
        /// </summary>
        /// <value>
        /// The runtime.
        /// </value>
        [JsonConverter(typeof(OmdbMovieRuntmeConverter))]
        public short? Runtime { get; set; }

        /// <summary>
        /// Gets or sets the genre.
        /// </summary>
        /// <value>
        /// The genre.
        /// </value>
        [JsonConverter(typeof(StringListJsonConverter))]
        public IEnumerable<string> Genre { get; set; }

        /// <summary>
        /// Gets or sets the director.
        /// </summary>
        /// <value>
        /// The director.
        /// </value>
        [JsonConverter(typeof(StringListJsonConverter))]
        public IEnumerable<string> Director { get; set; }

        /// <summary>
        /// Gets or sets the writer.
        /// </summary>
        /// <value>
        /// The writer.
        /// </value>
        [JsonConverter(typeof(StringListJsonConverter))]
        public IEnumerable<string> Writer { get; set; }

        /// <summary>
        /// Gets or sets the comma delimited list of actors.
        /// </summary>
        /// <value>
        /// The actors.
        /// </value>
        [JsonConverter(typeof(StringListJsonConverter))]
        public IEnumerable<string> Actors { get; set; }

        /// <summary>
        /// Gets or sets the plot.
        /// </summary>
        /// <value>
        /// The plot.
        /// </value>
        public string Plot { get; set; }

        /// <summary>
        /// Gets or sets the language.
        /// </summary>
        /// <value>
        /// The language.
        /// </value>
        [JsonConverter(typeof(StringListJsonConverter))]
        public IEnumerable<string> Language { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        [JsonConverter(typeof(StringListJsonConverter))]
        public IEnumerable<string> Country { get; set; }

        /// <summary>
        /// Gets or sets the awards.
        /// </summary>
        /// <value>
        /// The awards.
        /// </value>
        public string Awards { get; set; }

        /// <summary>
        /// Gets or sets the poster.
        /// </summary>
        /// <value>
        /// The poster.
        /// </value>
        public string Poster { get; set; }

        /// <summary>
        /// Gets or sets the list of ratings from various sources.
        /// </summary>
        /// <value>
        /// The ratings.
        /// </value>
        public List<Rating> Ratings { get; set; }

        /// <summary>
        /// Gets or sets the Metacritic metascore.
        /// </summary>
        /// <value>
        /// The metascore.
        /// </value>
        [JsonConverter(typeof(OmdbNumberJsonConverter))]
        public byte? Metascore { get; set; }

        /// <summary>
        /// Gets or sets the imdb rating.
        /// </summary>
        /// <value>
        /// The imdb rating.
        /// </value>
        [JsonConverter(typeof(OmdbNumberJsonConverter))]
        public float? ImdbRating { get; set; }

        /// <summary>
        /// Gets or sets the imdb votes.
        /// </summary>
        /// <value>
        /// The imdb votes.
        /// </value>
        [JsonConverter(typeof(OmdbNumberJsonConverter))]
        public long? ImdbVotes { get; set; }

        /// <summary>
        /// Gets or sets the imdb movie id.
        /// </summary>
        /// <value>
        /// The imdb movie id.
        /// </value>
        public string ImdbID { get; set; }

        /// <summary>
        /// Gets or sets the type of film.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the DVD release date.
        /// </summary>
        /// <value>
        /// The DVD.
        /// </value>
        [JsonConverter(typeof(OmdbDatetimeJsonConverter))]
        public DateTime? DVD { get; set; }

        /// <summary>
        /// Gets a value indicating whether the movie has been released on DVD.
        /// </summary>
        /// <value>
        ///   <c>true</c> if  movie has been released on DVD; otherwise, <c>false</c>.
        /// </value>
        public bool OnDvd => DVD.HasValue ? DVD.Value >= DateTime.Today : false;

        /// <summary>
        /// Gets or sets the box office revenue.
        /// </summary>
        /// <value>
        /// The box office.
        /// </value>
        [JsonConverter(typeof(OmdbDecimalJsonConverter))]
        public decimal? BoxOffice { get; set; }

        /// <summary>
        /// Gets or sets the production company.
        /// </summary>
        /// <value>
        /// The production.
        /// </value>
        public string Production { get; set; }

        /// <summary>
        /// Gets or sets the movie website.
        /// </summary>
        /// <value>
        /// The website.
        /// </value>
        public string Website { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the API's response was successful.
        /// </summary>
        /// <value>
        ///   <c>true</c> if response successful; otherwise, <c>false</c>.
        /// </value>
        public bool Response { get; set; }
    }

    /// <summary>
    /// A rating source and value
    /// </summary>
    public class Rating
    {
        /// <summary>
        /// Gets or sets the source.
        /// </summary>
        /// <value>
        /// The source.
        /// </value>
        public string Source { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public string Value { get; set; }

        /// <summary>
        /// Gets the number value.
        /// </summary>
        /// <value>
        /// The number value.
        /// </value>
        public byte NumberValue
        {
            get
            {
                if(Source == "Internet Movie Database")
                {
                    return Convert.ToByte(float.Parse(Value.Replace("/10", "")) * 10);
                }

                if(Source == "Rotten Tomatoes")
                {
                    return byte.Parse(Value.Replace("%", ""));
                }

                if (Source == "Metacritic")
                {
                    return byte.Parse(Value.Replace("/100", ""));
                }

                return 0;
            }
        }
    }

}
