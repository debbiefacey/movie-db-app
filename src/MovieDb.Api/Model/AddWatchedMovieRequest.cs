﻿using MovieDb.Data.Model;
using System;
using System.ComponentModel.DataAnnotations;

namespace MovieDb.Api.Model
{
    /// <summary>
    /// AddTrackedMovieRequest
    /// </summary>
    public class AddWatchedMovieRequest: WatchedMovieRequestBase
    {
        /// <summary>
        /// Gets or sets the movie.
        /// </summary>
        /// <value>
        /// The movie.
        /// </value>
        [Required]
        public Movie Movie { get; set; }
    }
}
