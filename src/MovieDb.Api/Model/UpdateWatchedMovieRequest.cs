﻿using MovieDb.Data.Model;
using System;
using System.ComponentModel.DataAnnotations;

namespace MovieDb.Api.Model
{
    /// <summary>
    /// UpdateTrackedMovieRequest
    /// </summary>
    public class UpdateWatchedMovieRequest: WatchedMovieRequestBase
    {
        /// <summary>
        /// Gets or sets the movie tracking id.
        /// </summary>
        /// <value>
        /// The movie tracking id.
        /// </value>
        [Required]
        public int id { get; set; }

    }
}
