﻿using MovieDb.Api.Json;
using Newtonsoft.Json;

namespace MovieDb.Api.Model
{
    /// <summary>
    /// Movie Summary
    /// </summary>
    public class OmdbMovieSummary
    {
        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the year.
        /// </summary>
        /// <value>
        /// The year.
        /// </value>
        [JsonConverter(typeof(OmdbNumberJsonConverter))]
        public short? Year { get; set; }

        /// <summary>
        /// Gets or sets the poster.
        /// </summary>
        /// <value>
        /// The poster.
        /// </value>
        public string Poster { get; set; }

        /// <summary>
        /// Gets or sets the imdb movie id.
        /// </summary>
        /// <value>
        /// The imdb movie id.
        /// </value>
        public string ImdbID { get; set; }

        /// <summary>
        /// Gets or sets the type of film.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public string Type { get; set; }
    }
}
