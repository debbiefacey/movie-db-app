﻿using MovieDb.Data.Model;
using System;
using System.ComponentModel.DataAnnotations;

namespace MovieDb.Api.Model
{
    /// <summary>
    /// UpdateTrackedMovieRequest
    /// </summary>
    public class WatchedMovieRequestBase
    {
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="TrackedMovie"/> is favorite.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this <see cref="TrackedMovie"/> is favorite; otherwise, <c>false</c>.
        /// </value>
        public bool IsFavorite { get; set; }

        /// <summary>
        /// Gets or sets the rating.
        /// </summary>
        /// <value>
        /// The last rating.
        /// </value>
        public byte Rating { get; set; }

        /// <summary>
        /// Gets or sets the watched date.
        /// </summary>
        /// <value>
        /// The last watched date.
        /// </value>
        public DateTimeOffset WatchedDate { get; set; }

        /// <summary>
        /// Gets or sets the comments.
        /// </summary>
        /// <value>
        /// The watched comments.
        /// </value>
        public string Comments { get; set; }

        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        public string UserId { get; set; }
    }
}
