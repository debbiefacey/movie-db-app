﻿using MovieDb.Data.Model;

namespace MovieDb.Api.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class AddMovieToWatchRequest
    {
        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the movie.
        /// </summary>
        /// <value>
        /// The movie.
        /// </value>
        public Movie Movie { get; set; }
    }
}
