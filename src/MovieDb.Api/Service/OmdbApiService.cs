﻿using MovieDb.Api.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace MovieDb.Api.Service
{
    /// <summary>
    /// Wrapper for OMDb API
    /// </summary>
    public class OmdbApiService : IOmdbApiService
    {
        private string omdbApiUrl = "http://www.omdbapi.com/?apikey=5100ecb0&type=movie&";

        /// <summary>
        /// Gets the movie by identifier.
        /// </summary>
        /// <param name="movieId">The movie identifier.</param>
        /// <returns></returns>
        public Task<(OmdbMovie movie, HttpStatusCode statusCode, string error)> GetMovieById(string movieId)
        {
            var url = $"{omdbApiUrl}i={movieId}";

            return GetMovieResult<OmdbMovie>(url);
        }

        /// <summary>
        /// Gets the movie by title and year.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="year">The year.</param>
        /// <returns></returns>
        public Task<(OmdbMovie movie, HttpStatusCode statusCode, string error)> GetMovieByTitleAndYear(string title, string year = null)
        {
            var url = $"{omdbApiUrl}t={title}&y={year}";

            return GetMovieResult<OmdbMovie>(url);
        }

        /// <summary>
        /// Searches movies by title and optionally year.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="year">The year.</param>
        /// <param name="page">The page.</param>
        /// <returns></returns>
        public async Task<(OmdbMovieSearchResult movies, HttpStatusCode statusCode, string error)> SearchMovie(string title, string year = null, int page = 1)
        {
            var url = $"{omdbApiUrl}s={title}&y={year}&page={page}";

            var result = await GetMovieResult<OmdbMovieSearchResult>(url);

            if (result.data != null)
            {
                result.data.Page = page;
            }

            return result;
        }

        private async Task<(T data, HttpStatusCode statusCode, string error)> GetMovieResult<T>(string url)
        {
            var client = new HttpClient();
            var response = await client.GetAsync(url);

            if (!response.IsSuccessStatusCode)
            {
                return (default(T), response.StatusCode, response.ReasonPhrase);
            }

            var json = await response.Content.ReadAsStringAsync();
            var jsonObject = JsonConvert.DeserializeObject(json) as JObject;

            var isResponseSuccessful = jsonObject.SelectToken("Response").Value<bool>();

            if (!isResponseSuccessful)
            {
                return (default(T), HttpStatusCode.NotFound ,jsonObject.SelectToken("Error").Value<string>());
            }

           return (jsonObject.ToObject<T>(), HttpStatusCode.OK, null);
        }
    }
}
