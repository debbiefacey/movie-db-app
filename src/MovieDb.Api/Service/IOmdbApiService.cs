﻿using System.Threading.Tasks;
using MovieDb.Api.Model;
using System.Net;
using System.Collections.Generic;

namespace MovieDb.Api.Service
{
    /// <summary>
    /// OMDb API wrapper service interface
    /// </summary>
    public interface IOmdbApiService
    {
        /// <summary>
        /// Gets the movie by identifier.
        /// </summary>
        /// <param name="movieId">The movie identifier.</param>
        /// <returns></returns>
        Task<(OmdbMovie movie, HttpStatusCode statusCode, string error)> GetMovieById(string movieId);

        /// <summary>
        /// Gets the movie by title and year.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="year">The year.</param>
        /// <returns></returns>
        Task<(OmdbMovie movie, HttpStatusCode statusCode, string error)> GetMovieByTitleAndYear(string title, string year = null);

        /// <summary>
        /// Searches movies by title and optionally year.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="year">The year.</param>
        /// <param name="page">The page.</param>
        /// <returns></returns>
        Task<(OmdbMovieSearchResult movies, HttpStatusCode statusCode, string error)> SearchMovie(string title, string year = null, int page = 1);
    }
}