﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using MovieDb.Data.Model;

namespace MovieDb.Data
{
    /// <summary>
    /// The Movie DbContext
    /// </summary>
    /// <seealso cref="DbContext" />
    public class MovieDbContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MovieDbContext"/> class.
        /// </summary>
        public MovieDbContext()
            : base()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MovieDbContext"/> class.
        /// </summary>
        /// <param name="options">The options.</param>
        public MovieDbContext(DbContextOptions<MovieDbContext> options)
            : base(options)
        {

        }

        /// <summary>
        /// Gets or sets the movies.
        /// </summary>
        /// <value>
        /// The movies.
        /// </value>
        public DbSet<Movie> Movies { get; set; }

        /// <summary>
        /// Gets or sets the tracked movies.
        /// </summary>
        /// <value>
        /// The tracked movies.
        /// </value>
        public DbSet<TrackedMovie> TrackedMovies { get; set; }

        /// <summary>
        /// Gets or sets the watched history.
        /// </summary>
        /// <value>
        /// The watched history.
        /// </value>
        public DbSet<WatchedDetail> WatchedHistory { get; set; }


        /// <inheritdoc />
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=localhost;Database=MovieDb;Trusted_Connection=True;MultipleActiveResultSets=true");
            }
        }

        /// <inheritdoc />
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Movie>().HasIndex(t => t.Title);
            modelBuilder.Entity<Movie>().HasIndex(t => t.Year);

            modelBuilder.Entity<TrackedMovie>()
                .HasDiscriminator<string>("TrackType")
                .HasValue<TrackedMovieToWatch>(TrackType.ToWatch.ToString())
                .HasValue<TrackedMovieWatched>(TrackType.Watched.ToString());

            modelBuilder.Entity<WatchedDetail>()
                .HasOne(typeof(TrackedMovieWatched))
                .WithMany(nameof(TrackedMovieWatched.WatchedHistory))
                .HasForeignKey(nameof(WatchedDetail.TrackedMovieId))
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
