﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace MovieDb.Data.Model
{
    /// <summary>
    /// Details of a movie a user tracks
    /// </summary>
    public class TrackedMovieWatched: TrackedMovie
    {
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="TrackedMovie"/> is favorite.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this <see cref="TrackedMovie"/> is favorite; otherwise, <c>false</c>.
        /// </value>
        public bool IsFavorite { get; set; }

        /// <summary>
        /// Gets or sets the last rating.
        /// </summary>
        /// <value>
        /// The last rating.
        /// </value>
        public byte LastRating => WatchedHistory.FirstOrDefault()?.Rating ?? 0;

        /// <summary>
        /// Gets or sets the last watched date.
        /// </summary>
        /// <value>
        /// The last watched date.
        /// </value>
        public DateTimeOffset? LastWatchedDate => WatchedHistory.FirstOrDefault()?.WatchedDate;


        /// <summary>
        /// Gets the last comments.
        /// </summary>
        /// <value>
        /// The last comments.
        /// </value>
        public string LastComments => WatchedHistory.FirstOrDefault()?.Comments;

        /// <summary>
        /// Gets or sets the watched history.
        /// </summary>
        /// <value>
        /// The watched history.
        /// </value>
        public ICollection<WatchedDetail> WatchedHistory { get; set; } = new List<WatchedDetail>();
    }
}
