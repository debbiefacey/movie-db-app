﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace MovieDb.Data.Model
{
    /// <summary>
    /// Details of a movie a user want to watch
    /// </summary>
    public class TrackedMovieToWatch : TrackedMovie
    {
    }
}
