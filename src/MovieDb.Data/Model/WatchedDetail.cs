﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MovieDb.Data.Model
{

    /// <summary>
    /// Detail of a movie that was watched
    /// </summary>
    public class WatchedDetail
    {

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [StringLength(32)]
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the movie identifier.
        /// </summary>
        /// <value>
        /// The movie identifier.
        /// </value>
        [StringLength(16)]
        public string MovieId { get; set; }

        /// <summary>
        /// Gets or sets the tracked movie identifier.
        /// </summary>
        /// <value>
        /// The tracked movie identifier.
        /// </value>
        public int TrackedMovieId { get; set; }

        /// <summary>
        /// Gets or sets the last rating.
        /// </summary>
        /// <value>
        /// The last rating.
        /// </value>
        public byte Rating { get; set; }

        /// <summary>
        /// Gets or sets the last watched date.
        /// </summary>
        /// <value>
        /// The last watched date.
        /// </value>
        public DateTimeOffset WatchedDate { get; set; }

        /// <summary>
        /// Gets or sets the comments.
        /// </summary>
        /// <value>
        /// The comments.
        /// </value>
        public string Comments { get; set; }
    }
}
