﻿namespace MovieDb.Data.Model
{
    /// <summary>
    /// Movie tracking type
    /// </summary>
    public enum TrackType
    {
        /// <summary>
        /// Flag movies to watch
        /// </summary>
        ToWatch,

        /// <summary>
        /// Flag movies watched
        /// </summary>
        Watched
    }
}
