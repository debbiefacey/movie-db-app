﻿using System.ComponentModel.DataAnnotations;

namespace MovieDb.Data.Model
{
    /// <summary>
    /// Movie Entity
    /// </summary>
    public class Movie
    {
        /// <summary>
        /// Gets or sets the movie id.
        /// </summary>
        /// <value>
        /// The movie id.
        /// </value>
        [StringLength(16)]
        public string MovieId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [Required]
        [StringLength(256)]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the poster URL.
        /// </summary>
        /// <value>
        /// The poster URL.
        /// </value>
        [Required]
        [StringLength(256)]
        public string Poster { get; set; }

        /// <summary>
        /// Gets or sets the year.
        /// </summary>
        /// <value>
        /// The year.
        /// </value>
        public short Year { get; set; }

        /// <summary>
        /// Gets or sets the trailer.
        /// </summary>
        /// <value>
        /// The trailer.
        /// </value>
        public string Trailer { get; set; }


        /// <summary>
        /// Gets or sets the tomato meter.
        /// </summary>
        /// <value>
        /// The tomato meter.
        /// </value>
        public byte TomatoMeter { get; set; }

        /// <summary>
        /// Gets or sets the imdb rating.
        /// </summary>
        /// <value>
        /// The imdb rating.
        /// </value>
        public byte ImdbRating { get; set; }
    }
}
