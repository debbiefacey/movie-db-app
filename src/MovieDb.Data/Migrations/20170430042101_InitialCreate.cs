﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace MovieDb.Data.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    MovieId = table.Column<string>(maxLength: 16, nullable: false),
                    ImdbRating = table.Column<byte>(nullable: false),
                    Poster = table.Column<string>(maxLength: 256, nullable: false),
                    Title = table.Column<string>(maxLength: 256, nullable: false),
                    TomatoMeter = table.Column<byte>(nullable: false),
                    Trailer = table.Column<string>(nullable: true),
                    Year = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.MovieId);
                });

            migrationBuilder.CreateTable(
                name: "TrackedMovies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MovieId = table.Column<string>(maxLength: 16, nullable: true),
                    TrackType = table.Column<string>(nullable: false),
                    UserId = table.Column<string>(maxLength: 32, nullable: true),
                    IsFavorite = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrackedMovies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TrackedMovies_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "MovieId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WatchedHistory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Comments = table.Column<string>(nullable: true),
                    MovieId = table.Column<string>(maxLength: 16, nullable: true),
                    Rating = table.Column<byte>(nullable: false),
                    TrackedMovieId = table.Column<int>(nullable: true),
                    UserId = table.Column<string>(maxLength: 32, nullable: true),
                    WatchedDate = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WatchedHistory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WatchedHistory_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "MovieId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WatchedHistory_TrackedMovies_TrackedMovieId",
                        column: x => x.TrackedMovieId,
                        principalTable: "TrackedMovies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Movies_Title",
                table: "Movies",
                column: "Title");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_Year",
                table: "Movies",
                column: "Year");

            migrationBuilder.CreateIndex(
                name: "IX_TrackedMovies_MovieId",
                table: "TrackedMovies",
                column: "MovieId");

            migrationBuilder.CreateIndex(
                name: "IX_WatchedHistory_MovieId",
                table: "WatchedHistory",
                column: "MovieId");

            migrationBuilder.CreateIndex(
                name: "IX_WatchedHistory_TrackedMovieId",
                table: "WatchedHistory",
                column: "TrackedMovieId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WatchedHistory");

            migrationBuilder.DropTable(
                name: "TrackedMovies");

            migrationBuilder.DropTable(
                name: "Movies");
        }
    }
}
