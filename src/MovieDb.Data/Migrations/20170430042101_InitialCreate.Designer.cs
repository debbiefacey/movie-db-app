﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using MovieDb.Data;

namespace MovieDb.Data.Migrations
{
    [DbContext(typeof(MovieDbContext))]
    [Migration("20170430042101_InitialCreate")]
    partial class InitialCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("MovieDb.Data.Model.Movie", b =>
                {
                    b.Property<string>("MovieId")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(16);

                    b.Property<byte>("ImdbRating");

                    b.Property<string>("Poster")
                        .IsRequired()
                        .HasMaxLength(256);

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasMaxLength(256);

                    b.Property<byte>("TomatoMeter");

                    b.Property<string>("Trailer");

                    b.Property<short>("Year");

                    b.HasKey("MovieId");

                    b.HasIndex("Title");

                    b.HasIndex("Year");

                    b.ToTable("Movies");
                });

            modelBuilder.Entity("MovieDb.Data.Model.TrackedMovie", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("MovieId")
                        .HasMaxLength(16);

                    b.Property<string>("TrackType")
                        .IsRequired();

                    b.Property<string>("UserId")
                        .HasMaxLength(32);

                    b.HasKey("Id");

                    b.HasIndex("MovieId");

                    b.ToTable("TrackedMovies");

                    b.HasDiscriminator<string>("TrackType").HasValue("TrackedMovie");
                });

            modelBuilder.Entity("MovieDb.Data.Model.WatchedDetail", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Comments");

                    b.Property<string>("MovieId")
                        .HasMaxLength(16);

                    b.Property<byte>("Rating");

                    b.Property<int?>("TrackedMovieId");

                    b.Property<string>("UserId")
                        .HasMaxLength(32);

                    b.Property<DateTimeOffset>("WatchedDate");

                    b.HasKey("Id");

                    b.HasIndex("MovieId");

                    b.HasIndex("TrackedMovieId");

                    b.ToTable("WatchedHistory");
                });

            modelBuilder.Entity("MovieDb.Data.Model.TrackedMovieToWatch", b =>
                {
                    b.HasBaseType("MovieDb.Data.Model.TrackedMovie");


                    b.ToTable("TrackedMovieToWatch");

                    b.HasDiscriminator().HasValue("ToWatch");
                });

            modelBuilder.Entity("MovieDb.Data.Model.TrackedMovieWatched", b =>
                {
                    b.HasBaseType("MovieDb.Data.Model.TrackedMovie");

                    b.Property<bool>("IsFavorite");

                    b.ToTable("TrackedMovieWatched");

                    b.HasDiscriminator().HasValue("Watched");
                });

            modelBuilder.Entity("MovieDb.Data.Model.TrackedMovie", b =>
                {
                    b.HasOne("MovieDb.Data.Model.Movie", "Movie")
                        .WithMany()
                        .HasForeignKey("MovieId");
                });

            modelBuilder.Entity("MovieDb.Data.Model.WatchedDetail", b =>
                {
                    b.HasOne("MovieDb.Data.Model.Movie", "Movie")
                        .WithMany()
                        .HasForeignKey("MovieId");

                    b.HasOne("MovieDb.Data.Model.TrackedMovieWatched", "TrackedMovie")
                        .WithMany("WatchedHistory")
                        .HasForeignKey("TrackedMovieId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
